#!/usr/bin/env python3

class DeferredOption:
    def __init__(self):
        self.value = None
    def __repr__(self):
        return repr(self.value)

option = DeferredOption()
