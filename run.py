#!/usr/bin/env python3

from configurator import setup_conf
import deferred
from pprint import pprint

def main():
    configuration = {}
    
    setup_conf(configuration)

    print("before:")
    pprint(configuration)
    
    deferred.option.value = 42
    
    print("after:")
    pprint(configuration)


if __name__ == "__main__":
    main()
