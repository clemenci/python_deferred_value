Simple example to show how to to pass around a value that gets assigned
in a different module (in practice a *common block*).

Just run `run.py`.
